from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

class InstagramBot:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.bot = webdriver.Firefox()

    def login(self):
        bot = self.bot
        bot.get('https://www.instagram.com/accounts/login/')
        time.sleep(3)

        username = bot.find_element_by_name('username')
        password = bot.find_element_by_name('password')
        username.clear()
        password.clear()

        username.send_keys(self.username)
        password.send_keys(self.password)
        password.send_keys(Keys.RETURN)
        time.sleep(3)

        btn = bot.find_element_by_class_name('HoLwm')
        btn.click()
        time.sleep(2)

    def like_post(self, hashtag):
        bot = self.bot
        bot.get('https://www.instagram.com/' + hashtag)
        time.sleep(2)

        for i in range(1, 5):
            bot.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(2)

        hrefs = bot.find_elements_by_tag_name('a')
        pic_hrefs = [elem.get_attribute('href') for elem in hrefs]

        for pic_href in pic_hrefs:
            bot.get(pic_href)
            bot.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            try:
                like = bot.find_element_by_class_name('glyphsSpriteHeart__outline__24__grey_9')
                like.click()
                time.sleep(5)
            except Exception as e:
                time.sleep(2)

test = InstagramBot('Test', 'Test')
test.login()
test.like_post('test')
